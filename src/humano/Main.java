package humano;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner lector = new Scanner(System.in);

        String nombre;
        int edad;
        char sexo;
        float peso;
        float altura;

        Persona humano1;
        Persona humano2;
        Persona humano3;

        System.out.println("Introduce tu nombre: ");
        nombre = lector.nextLine();
        System.out.println("Introduce tu edad: ");
        edad = lector.nextInt();
        System.out.println("Introduce tu sexo: ");
        sexo = lector.next().charAt(0);
        System.out.println("Introduce tu peso: ");
        peso = lector.nextFloat();
        System.out.println("Introduce tu altura: ");
        altura = lector.nextFloat();

        humano1 = new Persona(nombre, edad, sexo, peso, altura);
        humano2 = new Persona(nombre, edad, sexo);
        humano3 = new Persona();

        humano3.setNombre("Juansisco");
        humano3.setEdad(93);
        humano3.setSexo('H');
        humano3.setPeso(223);
        humano3.setAltura(3.64f);

        System.out.println(humano1);
        if (humano1.calcularIMC() == -1) {
            System.out.println("Estas por debajo del peso ideal");
        } else if (humano1.calcularIMC() == 0) {
            System.out.println("Estas en tu peso ideal");
        } else {
            System.out.println("Estas por encima del peso ideal");
        }
        if (humano1.esMayorEdad()) {
            System.out.println("Es mayor de edad");
        } else {
            System.out.println("No es mayor de edad");
        }

        System.out.println(humano2.toString());
        if (humano2.calcularIMC() == -1) {
            System.out.println("Estas por debajo del peso ideal");
        } else if (humano2.calcularIMC() == 0) {
            System.out.println("Estas en tu peso ideal");
        } else {
            System.out.println("Estas por encima del peso ideal");
        }
        if (humano2.esMayorEdad()) {
            System.out.println("Es mayor de edad");
        } else {
            System.out.println("No es mayor de edad");
        }

        System.out.println(humano3.toString());
        if (humano3.calcularIMC() == -1) {
            System.out.println("Estas por debajo del peso ideal");
        } else if (humano3.calcularIMC() == 0) {
            System.out.println("Estas en tu peso ideal");
        } else {
            System.out.println("Estas por encima del peso ideal");
        }
        if (humano3.esMayorEdad()) {
            System.out.println("Es mayor de edad");
        } else {
            System.out.println("No es mayor de edad");
        }
    }

}
