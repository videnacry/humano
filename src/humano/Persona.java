package humano;

import java.lang.Math;

public class Persona {

    private String nombre = "";
    private String DNI = "";
    private int edad = 0;
    private char sexo = 'H';
    private float peso = 0;
    private float altura = 0;

    public Persona() {
        this.DNI = generaDNI();
        this.sexo = comprobarSexo();
    }

    public Persona(String _nombre, int _edad, char _sexo) {
        this.nombre = _nombre;
        this.edad = _edad;
        this.sexo = _sexo;
        this.DNI = generaDNI();
        this.sexo = comprobarSexo();
    }

    public Persona(String _nombre, int _edad, char _sexo, float _peso, float _altura) {
        this.nombre = _nombre;
        this.DNI = generaDNI();
        this.edad = _edad;
        this.sexo = comprobarSexo();
        this.peso = _peso;
        this.altura = _altura;
    }

    public int calcularIMC() {
        int resultado;

        float IMC = peso / (altura * altura);

        if (IMC >= 18.5f && IMC <= 24.9f) {
            resultado = 0;
        } else if (IMC < 18.5f) {
            resultado = -1;
        } else {
            resultado = 1;
        }

        return resultado;
    }

    public boolean esMayorEdad() {
        if (this.edad >= 18) {
            return true;
        } else {
            return false;
        }
    }

    private char comprobarSexo() {
        if (sexo == 'H' || sexo == 'M') {

        } else {
            sexo = 'H';
        }
        return sexo;
    }

    @Override
    public String toString() {
        return "" + nombre + " " + DNI + " " + edad + " " + sexo + " " + peso + " " + altura;
    }

    private String generaDNI() {
        int numero = (int) (Math.random() * 100000000);
        int operacion = numero % 23;
        String resultado = "";

        switch (operacion) {
            case 0:
                resultado = "" + numero + 'T';
                break;
            case 1:
                resultado = "" + numero + 'R';
                break;
            case 2:
                resultado = "" + numero + 'W';
                break;
            case 3:
                resultado = "" + numero + 'A';
                break;
            case 4:
                resultado = "" + numero + 'G';
                break;
            case 5:
                resultado = "" + numero + 'M';
                break;
            case 6:
                resultado = "" + numero + 'Y';
                break;
            case 7:
                resultado = "" + numero + 'F';
                break;
            case 8:
                resultado = "" + numero + 'P';
                break;
            case 9:
                resultado = "" + numero + 'D';
                break;
            case 10:
                resultado = "" + numero + 'X';
                break;
            case 11:
                resultado = "" + numero + 'B';
                break;
            case 12:
                resultado = "" + numero + 'N';
                break;
            case 13:
                resultado = "" + numero + 'J';
                break;
            case 14:
                resultado = "" + numero + 'Z';
                break;
            case 15:
                resultado = "" + numero + 'S';
                break;
            case 16:
                resultado = "" + numero + 'Q';
                break;
            case 17:
                resultado = "" + numero + 'V';
                break;
            case 18:
                resultado = "" + numero + 'H';
                break;
            case 19:
                resultado = "" + numero + 'L';
                break;
            case 20:
                resultado = "" + numero + 'C';
                break;
            case 21:
                resultado = "" + numero + 'K';
                break;
            case 22:
                resultado = "" + numero + 'E';
                break;
            default:
                throw new AssertionError();
        }
        return resultado;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDNI() {
        return DNI;
    }

    public int getEdad() {
        return edad;
    }

    public char getSexo() {
        return sexo;
    }

    public float getPeso() {
        return peso;
    }

    public float getAltura() {
        return altura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

}
